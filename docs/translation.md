🇫🇷 🇬🇧 🇩🇪 🇨🇳 🇵🇹 🇭🇺 🇪🇸 🇮🇹 🇩🇰 🟩

➡️ The translations are handled in [our dedicated Weblate service](https://weblate.panoramax.xyz/). Any help is welcome.

!!! note "💬 Live chat"

	Check out the [Telegram group](https://t.me/panoramax_translation) to discuss with other translators

- Most of Panoramax can be translated. That includes all server-side software and interfaces displayed to the users.  
That means that all the text can be translated to any Weblate supported language, including local variations (e.g. 🇺🇸 American English VS 🇬🇧 British English).
- Some instance specific wording is not included in the translation (e.g. _Terms & services_). Please directly contact the instance manager for those.
- Localized formats for numbers, currency, dates, etc. is not currently supported.

### 🤣 Jokes
When the Panoramax website is loading, [18 random jokes are displayed](https://weblate.panoramax.xyz/translate/panoramax/web-viewer/en/?q=fun&sort_by=-priority%2Cposition&offset=1). Feel free to make funnier jokes in you own language!  
We suggest you discuss it with the local community to come up with the best jokes.

### ➕ Add a new language

For the first integration, the language translation needs to be 100% achieved.  
We will manually activate this first translation as soon as you make [a simple request on Gitlab](https://gitlab.com/panoramax/server/website/-/issues/new?issuable_template=language#).  

- The translation files are included from Weblate before each release. Feel free to keep translating the new strings in the future.
- For the upcoming integrations including small translations, the import is automated.

### 🗺️ Basemap

The basemap is specific to every instance.

- For the [OpenStreetMap France instance](https://panoramax.openstreetmap.fr), it is only translated into most spoken languages in the world: English 🇬🇧, French 🇫🇷, German 🇩🇪, Spanish 🇪🇸, Portugese 🇵🇹, Russian 🇷🇺, Chinese 🇨🇳.  The translation of places (countries and cities) is extracted from OpenStreetMap.  
The fallback language is English for all other languages.
- For the [IGN instance](https://panoramax.ign.fr), the only supported language is French 🇫🇷
- For other instances, the basemap is independently managed by their administrators.

### 🎁 External translations

#### OpenStreetMap login

Some instances such as [OpenStreetMap France](https://panoramax.openstreetmap.fr) rely on external login from [openstreetmap.org](https://openstreetmap.org) website.  
Therefore, you should make sure to translate some essential strings on this website too:

- [Log in to OpenStreetMap to access Panoramax](https://translatewiki.net/w/i.php?title=Special:Translations&message=Osm%3ASessions.new.login+to+authorize+html)
- [Authorize Panoramax to access your account with the following permissions?](https://translatewiki.net/w/i.php?title=Special:Translations&message=Osm%3AOauth2+authorizations.new.introduction)
- [Autorize](https://translatewiki.net/w/i.php?title=Special:Translations&message=Projects%3AOhm-oauth2+authorizations.new.authorize)
- [Deny](https://translatewiki.net/w/i.php?title=Special:Translations&message=Projects%3AOhm-oauth2+authorizations.new.deny)
- [read their user preferences](https://translatewiki.net/w/i.php?title=Special:Translations&message=Osm%3AActiverecord.attributes.client+application.allow+read+prefs)
- [By signing up, you agree to our Terms of Use, privacy policy and contributor terms](https://translatewiki.net/w/i.php?title=Special:Translations&message=Osm%3AUsers.new.by+signing+up.html)
- [I consider my contributions to be in the public domain](https://translatewiki.net/w/i.php?title=Special:Translations&message=Osm%3AUsers.terms.consider+pd)

#### Wikipedia pages

Feel free to create a Panoramax Wikipedia page in your own language from [the English article](https://en.wikipedia.org/wiki/Panoramax).
