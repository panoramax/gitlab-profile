There are many ways to contribute to Panoramax, here is a quick overview on different ways to join our community ✨

!!! note "Why contribute to Panoramax ?"

	Panoramax allows the contribution and reuse of field photos for many use cases such as: management of street furniture and roads, identification of traffic signs, etc.

	Learn more about the possible use cases in [our website](https://panoramax.fr/cas-d-usage).

## 💝 Share your pictures

The most obvious way to contribute to Panoramax is to share pictures. It's quite easy to do.

### 📷📱 Choose a camera

Take pictures - 360° or not - with a simple smartphone or any other more sophisticated device.

Panoramax have some requirements on the metadata associated to the picture:

- a position 📍
- a capture time ⏲️

Those metadata are usually automatically set by the camera (in what is called [exif tags](https://en.wikipedia.org/wiki/Exif)) if the camera has a means to get the position. For some complex use cases, those tags can also be given alongside the picture to the uploading API.

More metadata can be associated to the pictures, learn more about those in the [dedicated documentation section](https://panoramax.gitlab.io/gitlab-profile/pictures-metadata/).

For 360° pictures, there are quite a lot of available action cameras that have good quality. It's easier if the camera has an integrated GPS, else you will need an external GPS, or even better a [RTK receiver](https://en.wikipedia.org/wiki/Real-time_kinematic_positioning), for better accuracy.

!!! tip
    If you don't really know what to get, the community seems to agree with the GoPro Max as the current best value for money 360° camera.

Some tutorial have been gathered in the [french panoramax forum](https://forum.geocommuns.fr/t/les-tutoriels-pour-contribuer-a-panoramax/1015) (in 🇫🇷 french though).

### 📸 Take pictures

Panoramax accepts all street level pictures. They can be taken from a car 🚘, a bike 🚲, walking 🚶‍♀️ or whatever you want 🚣‍♂️🚆🛼.

Note that public Panoramax instances (see below) only accept pictures of the public space.

### 🌐 Choose where to share them

Panoramax is a decentralized system. Pictures are shared on what we call **panoramax instance** (see below how to create your own). Each instance will have its own set of rules and associated license.

For the moment there are 2 public instances, but we hope many more will spawn:

- [French Geography Institute (IGN) server](https://panoramax.ign.fr/)
- [OpenStreetMap France server](https://panoramax.openstreetmap.fr/)

### :material-upload: Upload them

The easiest way to upload the picture is to use the website of the chosen Panoramax instance. For more complex use cases, a command line tool is available, you can [check its documentation](https://panoramax.gitlab.io/gitlab-profile/cli/).

## 🖥️ Host you instance

You can check how to install a private or public Panoramax instance in the [API section](https://panoramax.gitlab.io/gitlab-profile/api/).

!!! note

	We'll try to add more tutorials to ease that process in the future.

If you want to host a public Panoramax instance, you can open an issue on the [federated catalog](https://gitlab.com/panoramax/server/meta-catalog/-/issues/new) (where all public pictures can be seen), so your instance can be harvested.

## :material-open-source-initiative: Contribute to the development

Panoramax is a fully open source initiative, founded mostly by public money, and will stay so as we are not a VC startup📈.

There is a lot to do, and we'll welcome any contributions, be it code, issues, documentation, translation...


<div class="grid cards" markdown>

-   :material-gitlab:{ .lg .middle } __Code__

    ---

    All the code is available in the [:material-gitlab: Panoramax organization](https://gitlab.com/panoramax/), you can check which component you want to contribute to and open a merge request or an issue on it.

-   🐛 __Bug report__

    ---

    If you see a bug and don't really know where to declare it, you can just [:octicons-file-16: open an issue on the main repository](https://gitlab.com/panoramax/gitlab-profile/-/issues/new), we'll dispatch it.

    
</div>

!!! tip
    You can also use [💬Panoramax Matrix channels](https://matrix.to/#/#panoramax-space:matrix.org) to discuss with the community

## 🗺️ Translate

Help us translate Panoramax, to start have a look [at our dedicated documentation](./translation.md).

## Use Panoramax

Discover and use photos shared by the community in the [federated catalog](https://api.panoramax.xyz/), and exploit them for your needs while respecting the pictures license.

<div class="grid cards" markdown>

-   :fontawesome-brands-js:{ .lg .middle } __Web viewer__

    ---

    Browse photos through our interactive web viewer and explore our entire photo catalog.

    This component can be integrated in a third party website or tool.
    
    [:octicons-arrow-right-24: Reference](https://panoramax.gitlab.io/gitlab-profile/web-viewer/01_Start/).

-   :material-api:{ .lg .middle } __API__

    ---

    Access photo data and metadata using our web API, compliant with [OGC Feature](https://ogcapi.ogc.org/features/) and [STAC](https://stacspec.org/en) standards.

    [:octicons-arrow-right-24: Reference](https://panoramax.gitlab.io/gitlab-profile/api/16_Using_API/).

</div>
