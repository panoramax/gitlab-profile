# ![Panoramax](https://upload.wikimedia.org/wikipedia/commons/thumb/a/a9/Panoramax.svg/40px-Panoramax.svg.png) Panoramax

Panoramax is a digital resource for sharing and using 📍📷 street pictures.

Anyone can take photographs of places visible from the public space and add them to the Panoramax database. This data is then freely accessible and reusable. It offers a similar service to StreetView, Mapillary, KartaView... but a with a completely open-source software stack, and fully managed by a growing open community.

![Panoramax usage, from taking pictures, uploading, and browsing or reusing images](./images/features.jpg)

➡️ **Give it a try** :

- [Federated catalog of all available pictures](https://panoramax.xyz/)
- [French Geography Institute (IGN) server](https://panoramax.ign.fr/)
- [OpenStreetMap France server](https://panoramax.openstreetmap.fr/)

## 📦 Components

Panoramax software is **modular** and made of several components, each of them standardized and ♻️ replaceable. Many of them are historically called _GeoVisio_, and can be used out of Panoramax context as well.

![GeoVisio architecture](./images/big_picture.png)

All of them are 📖 **open-source** and available on this Gitlab organization.

## 🤗 Special thanks

![Sponsors](./images/sponsors.png)

Panoramax and GeoVisio are made possible thanks to a group of ✨ **amazing** people ✨ :

- **[GéoVélo](https://geovelo.fr/)** team, for 💶 funding initial development and for 🔍 testing/improving software
- **[Carto Cité](https://cartocite.fr/)** team (in particular Antoine Riche), for 💶 funding improvements on viewer (map browser, flat pictures support)
- **[La Fabrique des Géocommuns (IGN)](https://www.ign.fr/institut/la-fabrique-des-geocommuns-incubateur-de-communs-lign)** for offering long-term support and funding the [Panoramax](https://panoramax.fr/) initiative and core team (Camille Salou, Mathilde Ferrey, Christian Quest, Antoine Desbordes, Jean Andreani, Adrien Pavie)
- **[Jungle Bus](https://junglebus.io/)** for UX testing and helping expanding the community
- Many _many_ **wonderful people** who worked on various parts of GeoVisio or core dependencies we use : 🧙 Stéphane Péneau, 🎚 Albin Calais & Cyrille Giquello, 📷 [Damien Sorel](https://www.strangeplanet.fr/), Pascal Rhod, Nick Whitelegg...
- **[Adrien Pavie](https://pavie.info/)**, for ⚙️ initial development of GeoVisio
- And you, ✨ **the amazing community** for making this project useful !
