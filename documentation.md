# Documentation

Documenting things is important !

General documentation is available in the `docs` folder of the repository. You can read it online, or access it locally:

```bash
# Install dependencies
pip install -e .

# Run with a local server
mkdocs serve
```
